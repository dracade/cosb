package com.dracade.cosb.api;

import com.dracade.cosb.core.punishments.Ban;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public interface Cosb {

    /**
     * Set the storage strategy.
     *
     * @param strategy The {@link CosbStorageStrategy} implementation
     */
    void setCosbStorageStrategy(CosbStorageStrategy strategy);

    /**
     * Retrieve the strategy accessor.
     *
     * @return The specified {@link CosbStorageStrategy}
     */
    CosbStorageStrategy getCosbStorageStrategy();

    /**
     * Cosb Utilities
     */
    class Utilities {

        /**
         * Retrieve the player's unique identifier.
         *
         * @param name The name of the player
         * @return The player's unique identifier
         */
        public static Optional<UUID> getPlayerId(String name) {
            UUID uniqueId = null;

            try {
                URL url = new URL("https://api.mojang.com/profiles/minecraft");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setUseCaches(true);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                OutputStream stream = connection.getOutputStream();
                stream.write(JSONArray.toJSONString(Arrays.asList(name)).getBytes());
                stream.flush();
                stream.close();

                JSONArray array = (JSONArray) new JSONParser().parse(new InputStreamReader(connection.getInputStream()));

                for (Object profile : array) {
                    JSONObject jsonProfile = (JSONObject) profile;
                    String id = (String) jsonProfile.get("id");
                    uniqueId = UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-" + id.substring(20, 32));
                }
            } catch (Exception ignored) {}

            return Optional.ofNullable(uniqueId);
        }

        /**
         * Retrieve the offence's pardon date from the specified arguments.
         *
         * @param args The arguments attempting to parse
         * @return The pardon date if one was specified
         *
         * @throws NumberFormatException Thrown if a parameter's value was not a number
         */
        public static Optional<Date> getPardonDateFromArguments(String... args) throws NumberFormatException {
            long pardonDate = System.currentTimeMillis();
            boolean dateSpecified = false;

            for (int i = 0; i < args.length; i++) {
                if (args[i].toLowerCase().startsWith("-r:")) {
                    break;
                } else if (args[i].toLowerCase().startsWith("-s:")) {
                    dateSpecified = true;
                    pardonDate += TimeUnit.SECONDS.toMillis(Integer.parseInt(args[i].substring(3, args[i].length())));
                } else if (args[i].toLowerCase().startsWith("-m:")) {
                    dateSpecified = true;
                    pardonDate += TimeUnit.MINUTES.toMillis(Integer.parseInt(args[i].substring(3, args[i].length())));
                } else if (args[i].toLowerCase().startsWith("-h:")) {
                    dateSpecified = true;
                    pardonDate += TimeUnit.HOURS.toMillis(Integer.parseInt(args[i].substring(3, args[i].length())));
                } else if (args[i].toLowerCase().startsWith("-d:")) {
                    dateSpecified = true;
                    pardonDate += TimeUnit.DAYS.toMillis(Integer.parseInt(args[i].substring(3, args[i].length())));
                } else if (args[i].toLowerCase().startsWith("-w:")) {
                    dateSpecified = true;
                    pardonDate += TimeUnit.DAYS.toMillis((Integer.parseInt(args[i].substring(3, args[i].length())) * 7));
                } else if (args[i].toLowerCase().startsWith("-m:")) {
                    dateSpecified = true;
                    Calendar now = Calendar.getInstance();
                    Calendar ahead = Calendar.getInstance();
                    ahead.add(Calendar.MONTH, Integer.parseInt(args[i].substring(3, args[i].length())));

                    pardonDate += ahead.getTimeInMillis() - now.getTimeInMillis();
                } else if (args[i].toLowerCase().startsWith("-y:")) {
                    dateSpecified = true;
                    Calendar now = Calendar.getInstance();
                    Calendar ahead = Calendar.getInstance();
                    ahead.add(Calendar.YEAR, Integer.parseInt(args[i].substring(3, args[i].length())));

                    pardonDate += ahead.getTimeInMillis() - now.getTimeInMillis();
                }
            }

            if (dateSpecified) {
                return Optional.of(new Date(pardonDate));
            } else {
                return Optional.empty();
            }
        }

        /**
         * Retrieve the offence's issue reason from the specified arguments.
         *
         * @param args The arguments attempting to parse
         * @return The offence's issue reason if one was specified
         */
        public static Optional<String> getReasonFromArguments(String... args) {
            String reason = "";
            boolean reasonSpecified = false;

            for (int i = 0; i < args.length; i++) {
                if (reasonSpecified) {
                    reason += args[i] + " ";
                } else {
                    if (args[i].toLowerCase().startsWith("-r:")) {
                        reason += args[i].substring(3, args[i].length()) + " ";
                        reasonSpecified = true;
                    }
                }
            }

            if (reasonSpecified) {
                return Optional.of(reason);
            } else {
                return Optional.empty();
            }
        }

        /**
         * Retrieve an offence's id from the specified arguments.
         *
         * @param args The arguments attempting to parse
         * @return The offence's id
         *
         * @throws NumberFormatException Thrown if a parameter's value was not a number
         */
        public static Optional<Long> getOffenceIdFromArguments(String... args) throws NumberFormatException {
            for (int i = 0; i < args.length; i++) {
                if (args[i].toLowerCase().startsWith("-id:")) {
                    return Optional.ofNullable(Long.parseLong(args[i].substring(4, args[i].length())));
                }
            }
            return Optional.empty();
        }

    }

}
