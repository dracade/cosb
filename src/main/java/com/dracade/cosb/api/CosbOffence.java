package com.dracade.cosb.api;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * An object which stores the information in regards
 * to an offence that has been committed.
 *
 * <p>An offence is created with a specified punishment
 * type. This type is used to help determine what
 * additional actions to take.
 *
 * @param <T> The type of {@link CosbPunishment} to inflict on the
 *            offender
 */
public class CosbOffence<T extends CosbPunishment> {

    private UUID offender, issuer;
    private Class<T> punishment;
    private Date issueDate, pardonDate;
    private boolean forcePardoned;
    private String reason;

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     */
    public CosbOffence(UUID offender, Class<T> punishment) {
        this.offender = offender;
        this.punishment = punishment;
        this.issueDate = new Date();
        this.forcePardoned = false;
    }

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     * @param issuer The unique identifier of the player who issued the
     *               offence
     */
    public CosbOffence(UUID offender, Class<T> punishment, UUID issuer) {
        this(offender, punishment);
        this.issuer = issuer;
    }

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     * @param reason The reason as to why the offence was issued
     */
    public CosbOffence(UUID offender, Class<T> punishment, String reason) {
        this(offender, punishment);
        this.reason = reason;
    }

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     * @param pardonDate The date of which this offence is to be treated as void
     */
    public CosbOffence(UUID offender, Class<T> punishment, Date pardonDate) {
        this(offender, punishment);
        this.pardonDate = pardonDate;
    }

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     * @param issuer The unique identifier of the player who issued the
     *               offence
     * @param reason The reason as to why the offence was issued
     */
    public CosbOffence(UUID offender, Class<T> punishment, UUID issuer, String reason) {
        this(offender, punishment, issuer);
        this.reason = reason;
    }

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     * @param issuer The unique identifier of the player who issued the
     *               offence
     * @param pardonDate The date of which this offence is to be treated as void
     */
    public CosbOffence(UUID offender, Class<T> punishment, UUID issuer, Date pardonDate) {
        this(offender, punishment, issuer);
        this.pardonDate = pardonDate;
    }

    /**
     * Create a new Offence instance.
     *
     * @param offender The unique identifier of the deserving entity
     * @param punishment The type of {@link CosbPunishment} to inflict for committing
     *                   the offence
     * @param issuer The unique identifier of the player who issued the
     *               offence
     * @param reason The reason as to why the offence was issued
     * @param pardonDate The date of which this offence is to be treated as void
     */
    public CosbOffence(UUID offender, Class<T> punishment, UUID issuer, String reason, Date pardonDate) {
        this(offender, punishment, issuer, reason);
        this.pardonDate = pardonDate;
    }

    /**
     * Retrieve the unique identifier of the
     * offender.
     *
     * @return The unique identifier of the offence's
     *         deserving entity.
     */
    public UUID getOffender() {
        return this.offender;
    }

    /**
     * Retrieve the issuer of the offence.
     *
     * <p>If no player issued the offence, then
     * the issuer should be treated as the server.
     *
     * @return The player's unique identifier who issued
     *         the offence
     */
    public Optional<UUID> getIssuer() {
        return Optional.ofNullable(this.issuer);
    }

    /**
     * Retrieve the punishment type for committing this offence.
     *
     * @return The {@link CosbPunishment} type
     */
    public Class<T> getPunishmentType() {
        return this.punishment;
    }

    /**
     * Retrieve the offence's issue date.
     *
     * @return The date of when the offence was issued
     */
    public Date getIssueDate() {
        return this.issueDate;
    }

    /**
     * Retrieve the offence's pardon date.
     *
     * <p>If no pardon date has been specified, then the
     * instance is to be treated as a permanent offence.
     *
     * @return The offence's pardon date if one has been specified
     */
    public Optional<Date> getPardonDate() {
        return Optional.ofNullable(this.pardonDate);
    }

    /**
     * Force pardon the offence.
     *
     * <p>This should be issued when the user's
     * offence is to be pardoned. Once this method
     * has been called, then the pardon cannot be undone.
     */
    public void pardon() {
        this.forcePardoned = true;
    }

    /**
     * Determine whether or not an offence has been pardoned.
     *
     * @return True if the offence has been pardoned
     */
    public boolean isPardoned() {
        return ((this.forcePardoned) || ((this.pardonDate != null) && (this.pardonDate.before(new Date()))));
    }

    /**
     * Set the offence's issue reason.
     *
     * @param reason The reason as to why the offence was issued
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Retrieve the offence's issue reason.
     *
     * <p>If no reason was specified, then a default
     * offence message should be specified, but isn't
     * required.
     *
     * @return The reason as to why the offence was issued
     */
    public Optional<String> getReason() {
        return Optional.ofNullable(this.reason);
    }

}
