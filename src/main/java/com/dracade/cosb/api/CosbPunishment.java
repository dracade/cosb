package com.dracade.cosb.api;

/**
 * An object which handles the taken on an offender after
 * committing an offence.
 */
public interface CosbPunishment {}
