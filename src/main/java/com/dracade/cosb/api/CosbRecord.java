package com.dracade.cosb.api;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A library of offences committed.
 *
 * @param <T> The type of {@link CosbPunishment} to filter through
 *            when searching for specific offences
 */
public class CosbRecord<T extends CosbPunishment> {

    private Comparator<CosbOffence> comparator;
    private UUID userId;
    private List<CosbOffence> offences;

    /**
     * Create a new Record instance.
     */
    public CosbRecord() {
        this.comparator = (a, b) -> {
            if (a.getIssueDate().after(b.getIssueDate())) {
                return 1;
            } else if (b.getIssueDate().after(a.getIssueDate())) {
                return -1;
            } else {
                return 0;
            }
        };
        this.offences = new ArrayList<CosbOffence>();
    }

    /**
     * Create a new Record instance.
     *
     * @param userId The user's unique identifier
     */
    public CosbRecord(UUID userId) {
        this();
        this.userId = userId;
    }

    /**
     * Create a new Record instance.
     *
     * @param offences The user's offences
     */
    public CosbRecord(CosbOffence... offences) {
        this();
        Collections.addAll(this.offences, offences);
    }

    /**
     * Create a new Record instance.
     *
     * @param userId The user's unique identifier
     * @param offences The user's offences
     */
    public CosbRecord(UUID userId, CosbOffence... offences) {
        this(userId);
        Collections.addAll(this.offences, offences);
    }

    /**
     * Retrieve the owner of the record.
     *
     * @return The user's unique identifier
     */
    public Optional<UUID> getRecordOwner() {
        return Optional.ofNullable(this.userId);
    }

    /**
     * Retrieve all of the offences.
     *
     * @return A list of offences
     */
    public List<CosbOffence> getAll() {
        return this.getAll(false);
    }

    /**
     * Retrieve all of the offences.
     *
     * @param activeOnly Active only offences
     * @return A list of offences
     */
    public List<CosbOffence> getAll(boolean activeOnly) {
        Collections.sort(this.offences, this.comparator);
        return this.offences.stream().filter(o -> ((!activeOnly) || (activeOnly && !o.isPardoned()))).collect(Collectors.toList());
    }

    /**
     * Retrieve all of the offences.
     *
     * @param punishment The punishment type to filter through
     * @return A list of offences
     */
    public List<CosbOffence> getAll(Class<T> punishment) {
        return this.getAll(false);
    }

    /**
     * Retrieve all of the offences.
     *
     * @param punishment The punishment type to filter through
     * @param activeOnly Active only offences
     * @return A list of offences
     */
    public List<CosbOffence> getAll(Class<T> punishment, boolean activeOnly) {
        Collections.sort(this.offences, this.comparator);
        return this.offences.stream().filter(o -> (o.getPunishmentType().isAssignableFrom(punishment) && ((!activeOnly) || (activeOnly && !o.isPardoned()))))
                .collect(Collectors.toList());
    }

    /**
     * Retrieve the latest offence.
     *
     * @return The latest offence committed
     */
    public Optional<CosbOffence> getLatest() {
        return this.getLatest(true);
    }

    /**
     * Retrieve the latest offence.
     *
     * @param punishment The punishment type to filter through
     * @return The latest offence committed
     */
    public Optional<CosbOffence> getLatest(Class<T> punishment) {
        return this.getLatest(punishment, true);
    }

    /**
     * Retrieve the latest offence.
     *
     * @return The latest offence committed
     */
    public Optional<CosbOffence> getLatest(boolean activeOnly) {
        List<CosbOffence> offences = this.getAll(activeOnly);
        return (offences.size() > 0) ? Optional.ofNullable(offences.get(offences.size() - 1)) : Optional.empty();
    }

    /**
     * Retrieve the latest offence.
     *
     * @param punishment The punishment type to filter through
     * @return The latest offence committed
     */
    public Optional<CosbOffence> getLatest(Class<T> punishment, boolean activeOnly) {
        List<CosbOffence> offences = this.getAll(punishment, activeOnly);
        return (offences.size() > 0) ? Optional.ofNullable(offences.get(offences.size() - 1)) : Optional.empty();
    }

}
