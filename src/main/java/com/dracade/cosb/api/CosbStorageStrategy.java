package com.dracade.cosb.api;

import java.util.Optional;
import java.util.UUID;

/**
 * An accessor interface which is required to be
 * implemented by Cosb adaptations.
 */
public interface CosbStorageStrategy {

    /**
     * Store an offence.
     *
     * @param offence The {@link CosbOffence) instance
     */
    void save(CosbOffence offence);

    /**
     * Delete an offence.
     *
     * @param offence The {@link CosbOffence} instance
     */
    void delete(CosbOffence offence);

    /**
     * Purge the entire record directory of all offences.
     */
    void purge();

    /**
     * Purge a user's offence record of all offences.
     *
     * @param userId The user's unique identifier
     */
    void purgeByOffender(UUID userId);

    /**
     * Purge an issuer's offence record of all offences.
     *
     * @param issuerId The user's unique identifier
     */
    void purgeByIssuer(UUID issuerId);

    /**
     * Retrieve an offence.
     *
     * @param offenceId The offence's identifier
     * @return The offence instance
     */
    Optional<CosbOffence> getOffence(long offenceId);

    /**
     * Retrieve an offence's id.
     *
     * @param offence The {@link CosbOffence} instance
     * @return The offence's identifier
     */
    Optional<Long> getOffenceId(CosbOffence offence);

    /**
     * Retrieve the entire record directory.
     *
     * <p>This should return a record containing
     * every single offence regardless of the
     * offender.
     *
     * @return A complete offence record
     */
    CosbRecord getRecord();

    /**
     * Retrieve a user's offence record.
     *
     * @param userId The user's unique identifier
     * @return A {@link CosbRecord) of the user's offences
     */
    CosbRecord getRecordByOffender(UUID userId);

    /**
     * Retrieve an issuer's offence record.
     *
     * @param issuerId The issuer's unique identifier
     * @return A {@link CosbRecord) of the user's issued offences
     */
    CosbRecord getRecordByIssuer(UUID issuerId);

}
