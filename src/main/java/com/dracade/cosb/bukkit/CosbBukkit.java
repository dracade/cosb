package com.dracade.cosb.bukkit;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.api.CosbRecord;
import com.dracade.cosb.api.CosbStorageStrategy;
import com.dracade.cosb.bukkit.commands.BukkitBanCommand;
import com.dracade.cosb.bukkit.commands.BukkitKickCommand;
import com.dracade.cosb.bukkit.commands.BukkitMuteCommand;
import com.dracade.cosb.bukkit.commands.BukkitPardonCommand;
import com.dracade.cosb.core.punishments.Ban;
import com.dracade.cosb.core.punishments.Mute;
import com.dracade.cosb.core.strategies.CosbMemoryStorageStrategy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public class CosbBukkit extends JavaPlugin implements Cosb, Listener {

    private static CosbBukkit instance;

    /**
     * Retrieve the api.
     *
     * @return Cosb api
     */
    public static Cosb api() {
        return CosbBukkit.instance;
    }

    /**
     * Retrieve the plugin.
     *
     * @return CosbBukkit plugin instance
     */
    public static CosbBukkit plugin() {
        return CosbBukkit.instance;
    }

    /**
     * Create a new CosbBukkit instance.
     *
     * @throws InstantiationException
     */
    public CosbBukkit() throws InstantiationException {
        if (CosbBukkit.instance != null) {
            throw new InstantiationException("CosbBukkit is a singleton instance.");
        } else {
            /**
             * Instantiate the plugin
             */
            CosbBukkit.instance = this;
        }
    }

    private CosbStorageStrategy strategy;

    @Override
    public final void onEnable() {
        this.strategy = new CosbMemoryStorageStrategy();

        this.getCommand("ban").setExecutor(BukkitBanCommand.getInstance());
        this.getCommand("kick").setExecutor(BukkitKickCommand.getInstance());
        this.getCommand("mute").setExecutor(BukkitMuteCommand.getInstance());
        this.getCommand("pardon").setExecutor(BukkitPardonCommand.getInstance());

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void setCosbStorageStrategy(CosbStorageStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public CosbStorageStrategy getCosbStorageStrategy() {
        return this.strategy;
    }

    @EventHandler
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        CosbRecord record = this.getCosbStorageStrategy().getRecordByOffender(event.getPlayer().getUniqueId());
        Optional<CosbOffence> mute = record.getLatest(Mute.class, true);
        if (mute.isPresent()) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You have been muted.");
        }
    }

    @EventHandler
    public void onAsyncPlayerPreLoginEvent(AsyncPlayerPreLoginEvent event) {
        CosbRecord record = this.getCosbStorageStrategy().getRecordByOffender(event.getUniqueId());
        Optional<CosbOffence> ban = record.getLatest(Ban.class, true);
        if (ban.isPresent()) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, (String) ban.get().getReason().orElse("The ban hammer has spoken!"));
        }
    }

}
