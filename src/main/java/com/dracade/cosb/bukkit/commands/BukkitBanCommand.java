package com.dracade.cosb.bukkit.commands;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.bukkit.CosbBukkit;
import com.dracade.cosb.core.punishments.Ban;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BukkitBanCommand implements CommandExecutor {

    private static BukkitBanCommand instance;

    /**
     * Retrieve the BukkitBanCommand instance.
     *
     * @return BukkitBanCommand instance
     */
    public static BukkitBanCommand getInstance() {
        return (BukkitBanCommand.instance != null) ? (BukkitBanCommand.instance) : (BukkitBanCommand.instance = new BukkitBanCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BukkitBanCommand instance.
     */
    private BukkitBanCommand() {
        this.cosb = CosbBukkit.api();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            Player p = Bukkit.getPlayer(args[0]);
            Optional<UUID> offender = (p != null ? Optional.of(p.getUniqueId()) : Cosb.Utilities.getPlayerId(args[0]));

            if (offender.isPresent()) {
                Optional<Date> pardonDate = Cosb.Utilities.getPardonDateFromArguments(args);
                Optional<String> issueReason = Cosb.Utilities.getReasonFromArguments(args);
                Optional<UUID> issuer;

                if (sender instanceof Player) {
                    issuer = Optional.of(((Player) sender).getUniqueId());
                } else {
                    issuer = Optional.empty();
                }

                CosbOffence<Ban> offence = new CosbOffence<Ban>(offender.get(), Ban.class, issuer.orElse(null), issueReason.orElse(null), pardonDate.orElse(null));
                this.cosb.getCosbStorageStrategy().save(offence);

                // Message user

            } else {
                // offender could not be found
            }

            return true;
        } catch (NumberFormatException e) {
            // incorrect usage
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }

        return false;
    }

}
