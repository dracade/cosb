package com.dracade.cosb.bukkit.commands;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.bukkit.CosbBukkit;
import com.dracade.cosb.core.punishments.Kick;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

public class BukkitKickCommand implements CommandExecutor {

    private static BukkitKickCommand instance;

    /**
     * Retrieve the BukkitKickCommand instance.
     *
     * @return BukkitKickCommand instance
     */
    public static BukkitKickCommand getInstance() {
        return (BukkitKickCommand.instance != null) ? (BukkitKickCommand.instance) : (BukkitKickCommand.instance = new BukkitKickCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BukkitKickCommand instance.
     */
    private BukkitKickCommand() {
        this.cosb = CosbBukkit.api();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            Player offender = Bukkit.getPlayer(args[0]);
            if (offender != null) {
                Optional<String> issueReason = Cosb.Utilities.getReasonFromArguments(args);
                Optional<UUID> issuer;

                if (sender instanceof Player) {
                    issuer = Optional.of(((Player) sender).getUniqueId());
                } else {
                    issuer = Optional.empty();
                }

                CosbOffence<Kick> offence = new CosbOffence<Kick>(offender.getUniqueId(), Kick.class, issuer.orElse(null), issueReason.orElse(null));
                this.cosb.getCosbStorageStrategy().save(offence);

                // Message user

            } else {
                // offender could not be found
            }

            return true;
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }

        return false;
    }

}
