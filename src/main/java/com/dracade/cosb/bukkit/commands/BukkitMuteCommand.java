package com.dracade.cosb.bukkit.commands;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.bukkit.CosbBukkit;
import com.dracade.cosb.core.punishments.Mute;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BukkitMuteCommand implements CommandExecutor {

    private static BukkitMuteCommand instance;

    /**
     * Retrieve the BukkitMuteCommand instance.
     *
     * @return BukkitMuteCommand instance
     */
    public static BukkitMuteCommand getInstance() {
        return (BukkitMuteCommand.instance != null) ? (BukkitMuteCommand.instance) : (BukkitMuteCommand.instance = new BukkitMuteCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BukkitMuteCommand instance.
     */
    private BukkitMuteCommand() {
        this.cosb = CosbBukkit.api();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            Player p = Bukkit.getPlayer(args[0]);
            Optional<UUID> offender = (p != null ? Optional.of(p.getUniqueId()) : Cosb.Utilities.getPlayerId(args[0]));

            if (offender.isPresent()) {
                Optional<Date> pardonDate = Cosb.Utilities.getPardonDateFromArguments(args);
                Optional<String> issueReason = Cosb.Utilities.getReasonFromArguments(args);
                Optional<UUID> issuer;

                if (sender instanceof Player) {
                    issuer = Optional.of(((Player) sender).getUniqueId());
                } else {
                    issuer = Optional.empty();
                }

                CosbOffence<Mute> offence = new CosbOffence<Mute>(offender.get(), Mute.class, issuer.orElse(null), issueReason.orElse(null), pardonDate.orElse(null));
                this.cosb.getCosbStorageStrategy().save(offence);

                // Message user

            } else {
                // offender could not be found
            }

            return true;
        } catch (NumberFormatException e) {
            // incorrect usage
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }

        return false;
    }

}

