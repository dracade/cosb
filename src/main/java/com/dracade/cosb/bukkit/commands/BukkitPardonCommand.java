package com.dracade.cosb.bukkit.commands;

import java.util.Optional;
import java.util.UUID;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.api.CosbPunishment;
import com.dracade.cosb.api.CosbRecord;
import com.dracade.cosb.bukkit.CosbBukkit;
import com.dracade.cosb.core.punishments.Ban;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BukkitPardonCommand implements CommandExecutor {

    private static BukkitPardonCommand instance;

    /**
     * Retrieve the BukkitPardonCommand instance.
     *
     * @return BukkitPardonCommand instance
     */
    public static BukkitPardonCommand getInstance() {
        return (BukkitPardonCommand.instance != null) ? (BukkitPardonCommand.instance) : (BukkitPardonCommand.instance = new BukkitPardonCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BukkitPardonCommand instance.
     */
    private BukkitPardonCommand() {
        this.cosb = CosbBukkit.api();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            Player p = Bukkit.getPlayer(args[0]);
            Optional<UUID> offender = (p != null ? Optional.of(p.getUniqueId()) : Cosb.Utilities.getPlayerId(args[0]));
            Optional<Long> offenceId = Cosb.Utilities.getOffenceIdFromArguments(args);

            if (offender.isPresent()) {
                CosbRecord record = this.cosb.getCosbStorageStrategy().getRecordByOffender(offender.get());
                Optional<CosbOffence> offence = Optional.empty();

                // Attempt to get the offence from it's identifier
                if (offenceId.isPresent()) {
                    offence = this.cosb.getCosbStorageStrategy().getOffence(offenceId.get());
                }

                // Attempt to get the latest offence if one was not found by it's identifier
                if (!offence.isPresent()) {
                    offence = record.getLatest(CosbPunishment.class, true);
                }

                // Pardon the offence
                if (offence.isPresent()) {
                    offence.get().pardon();

                    // Message user
                } else {
                    // no active offences
                }

            } else {
                // offender not found
            }

            return true;
        } catch (NumberFormatException e) {
            // not valid id
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }

        return false;
    }

}
