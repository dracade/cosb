package com.dracade.cosb.bungeecord;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.api.CosbRecord;
import com.dracade.cosb.api.CosbStorageStrategy;
import com.dracade.cosb.bungeecord.commands.BungeecordBanCommand;
import com.dracade.cosb.bungeecord.commands.BungeecordKickCommand;
import com.dracade.cosb.bungeecord.commands.BungeecordMuteCommand;
import com.dracade.cosb.bungeecord.commands.BungeecordPardonCommand;
import com.dracade.cosb.core.punishments.Ban;
import com.dracade.cosb.core.punishments.Mute;
import com.dracade.cosb.core.strategies.CosbMemoryStorageStrategy;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import org.bukkit.Bukkit;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.Optional;

public class CosbBungeecord extends Plugin implements Cosb, Listener {

    private static CosbBungeecord instance;

    /**
     * Retrieve the api.
     *
     * @return Cosb api
     */
    public static Cosb api() {
        return CosbBungeecord.instance;
    }

    /**
     * Retrieve the plugin.
     *
     * @return CosbBungeecord plugin instance
     */
    public static CosbBungeecord plugin() {
        return CosbBungeecord.instance;
    }

    /**
     * Create a new CosbBungeecord instance.
     *
     * @throws InstantiationException
     */
    public CosbBungeecord() throws InstantiationException {
        if (CosbBungeecord.instance != null) {
            throw new InstantiationException("CosbBungeecord is a singleton instance.");
        } else {
            /**
             * Instantiate the plugin
             */
            CosbBungeecord.instance = this;
        }
    }

    private CosbStorageStrategy strategy;

    @Override
    public final void onEnable() {
        this.strategy = new CosbMemoryStorageStrategy();

        this.getProxy().getPluginManager().registerCommand(this, BungeecordBanCommand.getInstance());
        this.getProxy().getPluginManager().registerCommand(this, BungeecordKickCommand.getInstance());
        this.getProxy().getPluginManager().registerCommand(this, BungeecordMuteCommand.getInstance());
        this.getProxy().getPluginManager().registerCommand(this, BungeecordPardonCommand.getInstance());

        this.getProxy().getPluginManager().registerListener(this, this);
    }

    @Override
    public void setCosbStorageStrategy(CosbStorageStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public CosbStorageStrategy getCosbStorageStrategy() {
        return this.strategy;
    }

    @EventHandler
    public void onChatEvent(ChatEvent event) {
        if (event.getSender() instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) event.getSender();
            CosbRecord record = this.getCosbStorageStrategy().getRecordByOffender(player.getUniqueId());
            Optional<CosbOffence> mute = record.getLatest(Mute.class, true);
            if (mute.isPresent()) {
                event.setCancelled(true);
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You have been muted."));
            }
        }
    }

    @EventHandler
    public void onAsyncPlayerPreLoginEvent(AsyncPlayerPreLoginEvent event) {
        CosbRecord record = this.getCosbStorageStrategy().getRecordByOffender(event.getUniqueId());
        Optional<CosbOffence> ban = record.getLatest(Ban.class, true);
        if (ban.isPresent()) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, (String) ban.get().getReason().orElse("The ban hammer has spoken!"));
        }
    }

}
