package com.dracade.cosb.bungeecord.commands;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.bungeecord.CosbBungeecord;
import com.dracade.cosb.core.punishments.Ban;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class BungeecordBanCommand extends Command {

    private static BungeecordBanCommand instance;

    /**
     * Retrieve the BungeecordBanCommand instance.
     *
     * @return BungeecordBanCommand instance
     */
    public static BungeecordBanCommand getInstance() {
        return (BungeecordBanCommand.instance != null) ? (BungeecordBanCommand.instance) : (BungeecordBanCommand.instance = new BungeecordBanCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BungeecordBanCommand instance.
     */
    private BungeecordBanCommand() {
        super("ban");
        this.cosb = CosbBungeecord.api();
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        try {
            ProxiedPlayer offender = CosbBungeecord.plugin().getProxy().getPlayer(args[0]);
            if (offender != null) {
                Optional<Date> pardonDate = Cosb.Utilities.getPardonDateFromArguments(args);
                Optional<String> issueReason = Cosb.Utilities.getReasonFromArguments(args);
                Optional<UUID> issuer;

                if (sender instanceof ProxiedPlayer) {
                    issuer = Optional.of(((ProxiedPlayer) sender).getUniqueId());
                } else {
                    issuer = Optional.empty();
                }

                CosbOffence<Ban> offence = new CosbOffence<Ban>(offender.getUniqueId(), Ban.class, issuer.orElse(null), issueReason.orElse(null), pardonDate.orElse(null));
                this.cosb.getCosbStorageStrategy().save(offence);

                // Message user

            } else {
                // offender could not be found
            }
        } catch (NumberFormatException e) {
            // incorrect usage
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }
    }

}
