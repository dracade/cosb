package com.dracade.cosb.bungeecord.commands;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.bungeecord.CosbBungeecord;
import com.dracade.cosb.core.punishments.Kick;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Optional;
import java.util.UUID;

public class BungeecordKickCommand extends Command {

    private static BungeecordKickCommand instance;

    /**
     * Retrieve the BungeecordKickCommand instance.
     *
     * @return BungeecordKickCommand instance
     */
    public static BungeecordKickCommand getInstance() {
        return (BungeecordKickCommand.instance != null) ? (BungeecordKickCommand.instance) : (BungeecordKickCommand.instance = new BungeecordKickCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BungeecordKickCommand instance.
     */
    private BungeecordKickCommand() {
        super("kick");
        this.cosb = CosbBungeecord.api();
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        try {
            ProxiedPlayer offender = CosbBungeecord.plugin().getProxy().getPlayer(args[0]);
            if (offender != null) {
                Optional<String> issueReason = Cosb.Utilities.getReasonFromArguments(args);
                Optional<UUID> issuer;

                if (sender instanceof ProxiedPlayer) {
                    issuer = Optional.of(((ProxiedPlayer) sender).getUniqueId());
                } else {
                    issuer = Optional.empty();
                }

                CosbOffence<Kick> offence = new CosbOffence<Kick>(offender.getUniqueId(), Kick.class, issuer.orElse(null), issueReason.orElse(null));
                this.cosb.getCosbStorageStrategy().save(offence);

                // Message user

            } else {
                // offender could not be found
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }
    }

}
