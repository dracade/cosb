package com.dracade.cosb.bungeecord.commands;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.bungeecord.CosbBungeecord;
import com.dracade.cosb.core.punishments.Mute;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class BungeecordMuteCommand extends Command {

    private static BungeecordMuteCommand instance;

    /**
     * Retrieve the BungeecordMuteCommand instance.
     *
     * @return BungeecordMuteCommand instance
     */
    public static BungeecordMuteCommand getInstance() {
        return (BungeecordMuteCommand.instance != null) ? (BungeecordMuteCommand.instance) : (BungeecordMuteCommand.instance = new BungeecordMuteCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BungeecordMuteCommand instance.
     */
    private BungeecordMuteCommand() {
        super("mute");
        this.cosb = CosbBungeecord.api();
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        try {
            ProxiedPlayer offender = CosbBungeecord.plugin().getProxy().getPlayer(args[0]);
            if (offender != null) {
                Optional<Date> pardonDate = Cosb.Utilities.getPardonDateFromArguments(args);
                Optional<String> issueReason = Cosb.Utilities.getReasonFromArguments(args);
                Optional<UUID> issuer;

                if (sender instanceof ProxiedPlayer) {
                    issuer = Optional.of(((ProxiedPlayer) sender).getUniqueId());
                } else {
                    issuer = Optional.empty();
                }

                CosbOffence<Mute> offence = new CosbOffence<Mute>(offender.getUniqueId(), Mute.class, issuer.orElse(null), issueReason.orElse(null), pardonDate.orElse(null));
                this.cosb.getCosbStorageStrategy().save(offence);

                // Message user

            } else {
                // offender could not be found
            }
        } catch (NumberFormatException e) {
            // incorrect usage
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }
    }

}
