package com.dracade.cosb.bungeecord.commands;

import com.dracade.cosb.api.Cosb;
import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.api.CosbPunishment;
import com.dracade.cosb.api.CosbRecord;
import com.dracade.cosb.bungeecord.CosbBungeecord;
import com.dracade.cosb.core.punishments.Ban;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class BungeecordPardonCommand extends Command {

    private static BungeecordPardonCommand instance;

    /**
     * Retrieve the BungeecordPardonCommand instance.
     *
     * @return BungeecordPardonCommand instance
     */
    public static BungeecordPardonCommand getInstance() {
        return (BungeecordPardonCommand.instance != null) ? (BungeecordPardonCommand.instance) : (BungeecordPardonCommand.instance = new BungeecordPardonCommand());
    }

    private Cosb cosb;

    /**
     * Create a new BungeecordPardonCommand instance.
     */
    private BungeecordPardonCommand() {
        super("pardon");
        this.cosb = CosbBungeecord.api();
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        try {
            ProxiedPlayer p = CosbBungeecord.plugin().getProxy().getPlayer(args[0]);
            Optional<UUID> offender = (p != null ? Optional.of(p.getUniqueId()) : Cosb.Utilities.getPlayerId(args[0]));
            Optional<Long> offenceId = Cosb.Utilities.getOffenceIdFromArguments(args);

            if (offender.isPresent()) {
                CosbRecord record = this.cosb.getCosbStorageStrategy().getRecordByOffender(offender.get());
                Optional<CosbOffence> offence = Optional.empty();

                // Attempt to get the offence from it's identifier
                if (offenceId.isPresent()) {
                    offence = this.cosb.getCosbStorageStrategy().getOffence(offenceId.get());
                }

                // Attempt to get the latest offence if one was not found by it's identifier
                if (!offence.isPresent()) {
                    offence = record.getLatest(CosbPunishment.class, true);
                }

                // Pardon the offence
                if (offence.isPresent()) {
                    offence.get().pardon();

                    // Message user
                } else {
                    // no active offences
                }
            } else {
                // offender not found
            }

        } catch (NumberFormatException e) {
            // not valid id
        } catch (ArrayIndexOutOfBoundsException e) {
            // incorrect usage
        }
    }

}
