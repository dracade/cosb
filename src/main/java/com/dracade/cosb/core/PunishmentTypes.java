package com.dracade.cosb.core;

import com.dracade.cosb.core.punishments.Ban;
import com.dracade.cosb.core.punishments.Kick;
import com.dracade.cosb.core.punishments.Mute;

/**
 * An object containing the built-in punishments.
 */
public final class PunishmentTypes {

    public static final Class<Ban> BAN = Ban.class;
    public static final Class<Kick> KICK = Kick.class;
    public static final Class<Mute> MUTE = Mute.class;

}
