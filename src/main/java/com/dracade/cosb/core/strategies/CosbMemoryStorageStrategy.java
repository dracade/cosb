package com.dracade.cosb.core.strategies;

import com.dracade.cosb.api.CosbOffence;
import com.dracade.cosb.api.CosbRecord;
import com.dracade.cosb.api.CosbStorageStrategy;
import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class CosbMemoryStorageStrategy implements CosbStorageStrategy {

    private List<CosbOffence> offences;

    /**
     * Create a new CosbMemoryStorageStrategy instance.
     */
    public CosbMemoryStorageStrategy() {
        this.offences = new ArrayList<CosbOffence>();
    }

    @Override
    public void save(CosbOffence offence) {
        Preconditions.checkNotNull(offence);
        if (!this.offences.contains(offence)) {
            this.offences.add(offence);
        }
    }

    @Override
    public void delete(CosbOffence offence) {
        Preconditions.checkNotNull(offence);
        if (this.offences.contains(offence)) {
            this.offences.remove(offence);
        }
    }

    @Override
    public void purge() {
        this.offences.clear();
    }

    @Override
    public void purgeByOffender(UUID userId) {
        Preconditions.checkNotNull(userId);
        this.offences.removeAll(this.offences.stream().filter(o -> (o.getOffender().equals(userId))).collect(Collectors.toList()));
    }

    @Override
    public void purgeByIssuer(UUID issuerId) {
        Preconditions.checkNotNull(issuerId);
        this.offences.removeAll(this.offences.stream().filter(o -> (o.getIssuer().orElse(null).equals(issuerId))).collect(Collectors.toList()));
    }

    @Override
    public Optional<CosbOffence> getOffence(long offenceId) {
        return Optional.ofNullable(this.offences.get((int) offenceId));
    }

    @Override
    public Optional<Long> getOffenceId(CosbOffence offence) {
        int id = this.offences.indexOf(offence);
        if (id >= 0) {
            return Optional.of((long) id);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public CosbRecord getRecord() {
        if (this.offences.size() > 0) {
            return new CosbRecord(this.offences.toArray(new CosbOffence[this.offences.size() - 1]));
        } else {
            return new CosbRecord();
        }
    }

    @Override
    public CosbRecord getRecordByOffender(UUID userId) {
        Preconditions.checkNotNull(userId);
        List<CosbOffence> offences = this.offences.stream().filter(o -> (o.getOffender().equals(userId))).collect(Collectors.toList());
        if (offences.size() > 0) {
            return new CosbRecord(offences.toArray(new CosbOffence[offences.size() - 1]));
        } else {
            return new CosbRecord();
        }
    }

    @Override
    public CosbRecord getRecordByIssuer(UUID issuerId) {
        Preconditions.checkNotNull(issuerId);
        List<CosbOffence> offences = this.offences.stream().filter(o -> (o.getIssuer().orElse(null).equals(issuerId))).collect(Collectors.toList());
        if (offences.size() > 0) {
            return new CosbRecord(offences.toArray(new CosbOffence[offences.size() - 1]));
        } else {
            return new CosbRecord();
        }
    }

}
